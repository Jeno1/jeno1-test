<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cars extends Model
{
    use HasFactory;

    protected $table = 'cars';

//    protected $primaryKey = 'car_id';

    protected $fillable = [
        'client_id', 'type', 'registered', 'ownbrand', 'accident'
    ];


}
