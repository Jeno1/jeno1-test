<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    use HasFactory;

    protected $table = 'services';

    protected $primaryKey = 'client_id';

    protected $fillable = [
        'car_id', 'lognumber', 'event', 'eventtime', 'document_id'
    ];
}
