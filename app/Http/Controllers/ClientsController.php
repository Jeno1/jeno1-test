<?php

namespace App\Http\Controllers;

use App\Models\Clients;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

    private function loadClients() {
        $json = \File::get(storage_path()."/json/clients.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            if (($obj->name != '') && ($obj->idcard != 0)) {
                \DB::table('clients')->insert(array(
                    'id' => $obj->id,
                    'name' => $obj->name,
                    'idcard' => $obj->idcard
                ));
            }
        }
    }

    private function loadCars() {
        $json = \File::get(storage_path()."/json/cars.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            if (($obj->client_id != 0) && ($obj->car_id != 0)) {
                \DB::table('cars')->insert(array(
                    'client_id' => $obj->client_id,
                    'car_id' => $obj->car_id,
                    'type' => $obj->type,
                    'registered' => $obj->registered,
                    'ownbrand' => $obj->ownbrand,
                    'accident' => $obj->accident
                ));
            }
        }
    }

    private function loadServices() {
        $json = \File::get(storage_path()."/json/services.json");
        $data = json_decode($json);

        foreach ($data as $obj) {
            if (($obj->client_id != 0) && ($obj->car_id != 0) && ($obj->lognumber != 0)) {
                if ($obj->eventtime == null) { 
//                    dd($obj);

                }
                \DB::table('services')->insert(array(
                    'client_id' => $obj->client_id,
                    'car_id' => $obj->car_id,
                    'lognumber' => $obj->lognumber,
                    'event' => $obj->event,
                    'eventtime' => $obj->eventtime,
                    'document_id' => $obj->document_id
                ));
            }
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {


        /*
        \App\Models\Clients::truncate();
        \App\Models\Services::truncate();
        \App\Models\Cars::truncate();
        */


        $clients = \App\Models\Clients::all();
        $cars = \App\Models\Cars::all();
        $services = \App\Models\Services::all();

        if (
            (sizeof($clients)==0) ||
            (sizeof($cars)==0) ||
            (sizeof($services)==0)
        ) {

//            dd("Must reload stuff from JSON");

            $this->loadClients();
            $this->loadCars();
            $this->loadServices();

            $clients = \App\Models\Clients::all();

        }

        return view('index', ['clients' => $clients]);
    }

    public function getClientCars($client_id) {

        $clientCars = \App\Models\Cars::where('client_id', '=', $client_id)->orderBy("accident", "desc")->get();
        foreach ($clientCars as $key=>$value) {
            $additionalDetails = \App\Models\Services::where('client_id', '=', $value->client_id)->where('car_id', "=", $value->car_id)->orderBy("lognumber", "desc")->limit(1)->get();
            $clientCars[$key]["lognumber"] = $additionalDetails[0]->lognumber;
            $clientCars[$key]["event"] = $additionalDetails[0]->event;
            $clientCars[$key]["eventtime"] = $additionalDetails[0]->eventtime;
        }

        return json_encode($clientCars);
    }

    public function getClientCarsHistory($client_id, $car_id) {

        $clientCarsHistory = \App\Models\Services::where('client_id', '=', $client_id)->where("car_id", "=", $car_id)->orderBy("eventtime", "desc")->get();

        return json_encode($clientCarsHistory);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function show(Clients $clients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function edit(Clients $clients)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clients $clients)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Clients  $clients
     * @return \Illuminate\Http\Response
     */
    public function destroy(Clients $clients)
    {
        //
    }
}
