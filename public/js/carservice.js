function drawTable(input, client_id) {

    data = jQuery.parseJSON(input);

    ret = '';
    for (i=0; i<data.length; i++) {
        ret+= '<div class="clientInfo">';
        ret+= '<span style="font-weight: bold;" onclick="getClientCarsHistory('+client_id+', '+data[i].car_id+')">Car ID: '+data[i].car_id+'</span><br>';
        ret+= 'Type: '+data[i].type+'<br>';
        ret+= 'Accidents: '+data[i].accident+'<br>';
        ret+= 'Own brand: '+data[i].ownbrand+'<br>';
        ret+= 'Registered: '+data[i].registered+'<br>';
        ret+= 'Lognumber: '+data[i].lognumber+'<br>';
        ret+= 'Event: '+data[i].event+'<br>';
        ret+= 'Event time: '+data[i].eventtime+'<br>';
        ret+='<div class="historyContainer" id="client_id_'+ client_id + '_' + data[i].car_id+'" class="col-sm-12 left hidden"></div>';
        ret+= '</div>';
    }

    return ret;
}

function drawHistoryTable(input) {

    data = jQuery.parseJSON(input);

    ret='<div class="historyInfo">';
    for (i=0; i<data.length; i++) {
        ret+= '<div class="historyItem">';
        ret+= 'Log#: '+data[i].lognumber+'<br>';
        ret+= 'Event: '+data[i].event+'<br>';
        ret+= 'Event time: '+data[i].eventtime+'<br>';
        ret+= 'Document ID: '+data[i].document_id+'<br>';
        ret+= '</div>';
    }
    ret+='</div>';

    return ret;
}

function getClientCarsHistory(client_id, car_id) {
    jQuery.ajax({
        url: "/ajax/getClientCarsHistory/"+client_id+"/"+car_id,
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        },
        method: 'get',
        success: function(result){
            $('#client_id_'+client_id+'_'+car_id).html(drawHistoryTable(result));
            $('#client_id_'+client_id+'_'+car_id).show();
    }});

}

function getClientCars(client_id) {

    if ($('#client_id_'+client_id).is(':visible')) {
            $('#client_id_'+client_id).toggle();
    } else {

        jQuery.ajax({
            url: "/ajax/getClientCars/"+client_id,
            headers: {
              'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            },
            method: 'get',
            success: function(result){
                if (result=="[]") {
//                    window.alert('Nincs rögzített adat.');
                    Swal.fire(
                      'Hiba!',
                      'Nincs rögzített adat.',
                      'success'
                    );

                }
            $('#client_id_'+client_id).html(drawTable(result, client_id));
            $('#client_id_'+client_id).toggle();
        }});
    }
}
