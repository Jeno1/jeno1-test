<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App\Models;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    $clients=Clients::index();
//    return view('index');

    return App::call('\App\Http\Controllers\ClientsController@index');


});


Route::get('/ajax/getClientCars/{client_id}', ['uses' => '\App\Http\Controllers\ClientsController@getClientCars']);

Route::get('/ajax/getClientCarsHistory/{client_id}/{car_id}', ['uses' => '\App\Http\Controllers\ClientsController@getClientCarsHistory']);

